# dependencies
rustup, sfml, csfml (`sudo pacman -S rustup sfml csfml`)
# build instructions
just run `cargo build` and `cargo run`
# gameplay
vi keys (left, down, up, right => h, j, k, l) to move

close window to exit (alt+f4)
