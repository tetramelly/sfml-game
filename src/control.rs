/*
Copyright Dani 2019
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::time;

use sfml::system::Vector2;
use sfml::graphics::Sprite;
use sfml::graphics::Transformable;
use sfml::window;

// find out how to use a transformable object rather than a RectangleShape later
pub fn control(actor: &mut Sprite, elapsed: time::Duration) {
    let mut vel = Vector2::new(0.0, 0.0);
    if window::Key::L.is_pressed() {
        vel += move_right(actor.position().x);
    }
    if window::Key::K.is_pressed() {
        vel += move_up(actor.position().y);
    }
    if window::Key::J.is_pressed() {
        vel += move_down(actor.position().y);
    }
    if window::Key::H.is_pressed() {
        vel += move_left(actor.position().x);
    }
    actor.move_(elapsed.as_secs_f32() * 300.0 * vel);
}

fn move_right(x: f32) -> Vector2<f32> {
    return if x >= 960.0 {Vector2::new(0.0, 0.0)} else {Vector2::new(1.0, 0.0)}
}

fn move_up(y: f32) -> Vector2<f32> {
    return if y <= 0.0 {
        Vector2::new(0.0, 0.0)
    } else {
        Vector2::new(0.0, -1.0)
    };
}

fn move_down(y: f32) -> Vector2<f32> {
    return if y >= 400.0 {Vector2::new(0.0, 0.0)} else {Vector2::new(0.0, 1.0)}
}

fn move_left(x: f32) -> Vector2<f32> {
    return if x <= 0.0 {
        Vector2::new(0.0, 0.0)
    } else {
        Vector2::new(-1.0, 0.0)
    };
}
