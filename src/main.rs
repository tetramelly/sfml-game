/*
Copyright Dani 2019
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::time;

use sfml::graphics;
use sfml::window;

use control::control;

use sfml::graphics::RenderTarget;

mod control;
mod sprites;

fn main() {
    let width = 1280;
    let height = 720;
    let mut window = graphics::RenderWindow::new(window::VideoMode::new(width, height, 24),
                                                 "sfml game",
                                                 window::Style::CLOSE,
                                                 &window::ContextSettings::default());
    window.set_vertical_sync_enabled(true);

    let texture = sprites::get_texture("img/cboy.png");
    let mut character = graphics::Sprite::with_texture(&texture);

    let mut old = time::Instant::now();

    while window.is_open() {
        if let Some(event) = window.poll_event() {
            match event {
                window::Event::Closed | window::Event::KeyPressed {code: window::Key::Escape, ..} => window.close(),
                _ => {}
            }
        }

        let elapsed = time::Instant::now().duration_since(old);
        old = time::Instant::now();

        control(&mut character, elapsed);

        window.clear(graphics::Color::WHITE);
        window.draw(&character);
        window.display();
    }
}
